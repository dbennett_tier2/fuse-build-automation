#!/usr/bin/env groovy

def defaultConfig(name, label) {
    return [
        application: [
            name: name,
            label: label
        ],
        mvn: [
            task: 'package',
            settings: 'configuration/settings-osmo.xml',
            params: ''
        ],
        routes: [
            [
                name: name,
                label: label,
                path: '/',
                service: [
                    name: name,
                    port: '10000'
                ]
            ]
        ],
        source: [
            repositoryPath: "bitbucket.org/motabilityoperations/fuse-${name}.git",
            repositoryBranch: "refs/tags/${env.IMAGE_VERSION}",
            repositoryCredentials: 'bitbucket-ro'
        ],
        imageRepositoryUrl: "nexus-${env.SOURCE_ENVIRONMENT}-devops.services.theosmo.com/mo",
        openshiftNamespace: "fuse-${env.TARGET_ENVIRONMENT ?: env.SOURCE_ENVIRONMENT}",
        slackChannel: '#fuse-builds-status'
    ]
}
def deploy(cfg, currentBuild) {
    try {
        node('mo-aio') {
            // get latest version of source code from source control for template files
            stage('BUILD: Checkout Source') {
                checkout scm: [
                    $class: 'GitSCM',
                    userRemoteConfigs: [[
                        url: "https://${cfg.source.repositoryPath}",
                        credentialsId: cfg.source.repositoryCredentials
                    ]],
                    branches: [[
                        name: "${cfg.source.repositoryBranch}"
                    ]]
                ], poll: false
            }
            stage('ENV: Deploy') {
                importImage(cfg)
                updateDeploymentResources(cfg)
                deploy(cfg, 2)
            }
        }
        currentBuild.result = 'SUCCESS'
    } catch (exception) {
        currentBuild.result = 'FAILURE'
        throw exception
    } finally {
        // send message to Slack
        // notifySlack(cfg currentBuild.result)
    }
}

def updateDeploymentResources(cfg) {

    String openshiftNamespace = cfg.openshiftNamespace
    String imageStream = sh script: "oc get is ${cfg.application.name} --template {{.status.dockerImageRepository}} --namespace ${openshiftNamespace}", returnStdout: true

    Map< String, Object > deployConfigParameters = [
        DEPLOYMENT_NAME: cfg.application.name,
        DEPLOYMENT_LABEL: cfg.application.label,
        APPLICATION_NAME: cfg.application.name,
        IMAGE_STREAM: imageStream,
        IMAGE_TAG: env.IMAGE_VERSION
    ]

    println "Creating deployment on $openshiftNamespace from config: $deployConfigParameters"
    applyResourceFromTemplate(openshiftNamespace, 'configuration/templates/deployment-config.yaml', deployConfigParameters)
}

def importImage(cfg) {
    println "Importing image from: ${cfg.imageRepositoryUrl}/${cfg.application.name}:${cfg.application.version}"
    sh "oc import-image --from=${cfg.imageRepositoryUrl}/${cfg.application.name}:${env.IMAGE_VERSION} --confirm ${cfg.application.name}:${env.IMAGE_VERSION} --namespace ${cfg.openshiftNamespace}"
}

def deployToOpenShift(cfg, replicas = 1) {

    sh "oc rollout latest dc/${cfg.application.name} --namespace ${cfg.openshiftNamespace}"
    sh "oc rollout status --watch dc/${cfg.application.name} --namespace ${cfg.openshiftNamespace}"

    if (replicas > 1) {
        sh "oc scale --replicas=${replicas} dc/${cfg.application.name} --namespace ${cfg.openshiftNamespace}"
    }

    sh "oc set triggers dc/${cfg.application.name} --from-config=true --namespace ${cfg.openshiftNamespace}"
}

@NonCPS
def applyResourceFromTemplate(openshiftNamespace, resourcePath, parameters) {

    String command = "oc process --filename ${resourcePath} --namespace ${openshiftNamespace}"
    parameters.each { String key, Object value ->
        if (value != null) {
            command += " --param=${key}=${value}"
        }
    }
    command += " | oc apply --namespace ${openshiftNamespace} --filename -"
    sh "${command}"
}

def notifySlack(cfg, buildStatus = 'STARTED') {

    String colour
    switch (buildStatus) {
        case 'STARTED':
            colour = '#D4DADF'
            break
        case 'APPROVAL':
            colour = '#0072BC'
            break
        case 'SUCCESS':
            colour = '#BDFFC3'
            break
        case 'UNSTABLE':
            colour = '#FFFE89'
            break
        default:
            colour = '#FF9FA1'
            break
    }

    def resultBefore = currentBuild.result
    def lastBuildStatus = currentBuild.getPreviousBuild()?.getResult()

    // Only send if current build is not SUCCESS or current build is Back to Green following a failure
    boolean sendSlack = (buildStatus != 'SUCCESS' || (buildStatus == 'SUCCESS' && lastBuildStatus != 'SUCCESS'))
    if (sendSlack) {
        try {
            String message = "${buildStatus}: `${env.JOB_NAME}` #${env.BUILD_NUMBER}:\n<${env.JENKINS_URL}blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline|Click Here To View Pipeline>"
            if (buildStatus == 'SUCCESS') {
                message += "\nPrevious Build Status: ${lastBuildStatus}\nBuild is back to GREEN!"
            }
            slackSend(channel: cfg.slackChannel, color: colour, message: message)
        } catch (exception) {
            currentBuild.result = resultBefore
        }
    }
}

#!/usr/bin/env groovy

def defaultConfig(name, label) {
    return [
        application: [
            name: name,
            label: label
        ],
        mvn: [
            settings: 'configuration/settings-osmo.xml',
            build: [
                task: 'clean package',
                params: '-Dmaven.test.skip=true'
            ],
            test: [
                task: 'test',
                params: '-Dtest=!*Steps,!BDDTestRunner'
            ],
            bdd: [
                task: 'surefire:test',
                params: '-Dtest=BDDTestRunner'
            ]
        ],
        routes: [
            [
                name: name,
                label: label,
                path: '/',
                service: [
                    name: name,
                    port: '10000'
                ]
            ]
        ],
        additionalBuildParams: [:],
        additionalDeploymentParams: [:],
        additionalServiceParams: [:],
        configMaps: [
            dev: [
                name : name,
                label: name,
                params: [
                    SERVICE_HOST: '0.0.0.0',
                    SERVICE_PORT: '10000'
                ]
            ],
            e2e: [
                name : name,
                label: name,
                params: [
                    SERVICE_HOST: '0.0.0.0',
                    SERVICE_PORT: '10000'
                ]
            ]
        ],
        source: [
            repositoryPath: "bitbucket.org/motabilityoperations/fuse-${name}.git",
            repositoryBranch: 'fuse-upgrade',
            repositoryCredentials: 'bitbucket-rw'
        ],
        artifactRepositoryCredentials: 'nexus-ro',
        imageRepositoryUrl: 'nexus-dev-devops.services.theosmo.com/mo',
        openshiftNamespaces: [
            dev: 'fuse-dev',
            e2e: 'fuse-e2e'
        ],
        onePassword: [
            secretNamespace: 'devops',
            secretName: '1password',
        ],
        slackChannel: '#fuse-builds-status'
    ]
}

def build(cfg, currentBuild) {

    try {

        node('mo-aio') {
            // get latest version of source code from source control
            stage('BUILD: Checkout Source') {
                git url: "https://${cfg.source.repositoryPath}", branch: cfg.source.repositoryBranch, credentialsId: cfg.source.repositoryCredentials
                pom = readMavenPom file: 'pom.xml'
                cfg.application.version = "${pom.version}-b${env.BUILD_NUMBER}"
            }
            // build artifact
            stage('BUILD: Build Artifact') {
                withCredentials([usernamePassword(credentialsId: cfg.artifactRepositoryCredentials,
                    usernameVariable: 'ARTIFACT_REPOSITORY_USERNAME',
                    passwordVariable: 'ARTIFACT_REPOSITORY_PASSWORD')
                ]) {
                    sh "mvn ${cfg.mvn.build.task} -s ${cfg.mvn.settings} ${cfg.mvn.build.params}"
                }
            }
            // run tests
            stage('BUILD: Run Unit and IT Tests') {
                withCredentials([usernamePassword(credentialsId: cfg.artifactRepositoryCredentials,
                    usernameVariable: 'ARTIFACT_REPOSITORY_USERNAME',
                    passwordVariable: 'ARTIFACT_REPOSITORY_PASSWORD')
                ]) {
                    sh "mvn ${cfg.mvn.test.task} -s ${cfg.mvn.settings} ${cfg.mvn.test.params}"
                }
            }
            // build image and deploy to image repository
            stage('Build Image') {
                updateBuildResources(cfg, 'dev')
                sh "oc start-build ${cfg.application.name} --from-dir=. --follow --namespace ${cfg.openshiftNamespaces.dev}"
            }
            // prepare DEV config maps and secrets
            stage('DEV: Prepare Config') {
                updateConfigMapResources(cfg, 'dev')
            }
            // download image from image repository and deploy to DEV
            stage('DEV: Deploy') {
                importImage(cfg, 'dev')
                updateDeploymentResources(cfg, 'dev')
                updateServiceResources(cfg, 'dev')
                updateRouteResources(cfg, 'dev')
                deployToOpenShift(cfg, 'dev', 1)
            }
            // run BDD tests after deploying to DEV
            stage('DEV: Run BDD Tests') {
                withCredentials([usernamePassword(credentialsId: cfg.artifactRepositoryCredentials,
                    usernameVariable: 'ARTIFACT_REPOSITORY_USERNAME',
                    passwordVariable: 'ARTIFACT_REPOSITORY_PASSWORD')
                ]) {
                    sh "mvn ${cfg.mvn.bdd.task} -s ${cfg.mvn.settings} ${cfg.mvn.bdd.params}"
                }
            }
            // prepare DEV config maps and secrets
            stage('E2E: Prepare Config') {
                updateConfigMapResources(cfg, 'dev')
            }
            // download image from image repository and deploy to E2E
            stage('E2E: Deploy') {
                updateConfigMapResources(cfg, 'e2e')
                importImage(cfg, 'e2e')
                updateDeploymentResources(cfg, 'e2e')
                updateServiceResources(cfg, 'e2e')
                updateRouteResources(cfg, 'e2e')
                deployToOpenShift(cfg, 'e2e', 2)
            }
            // tag source with build number
            stage('BUILD: Tag Source') {
                withCredentials([usernamePassword(credentialsId: cfg.source.repositoryCredentials,
                    usernameVariable: 'sourceRepositoryUsername',
                    passwordVariable: 'sourceRepositoryPassword')
                ]) {
                    sh "git tag ${cfg.application.version}"
                    sh "git push https://${sourceRepositoryUsername}:${sourceRepositoryPassword}@${cfg.source.repositoryPath} ${cfg.application.version}"
                }
            }
        }
        currentBuild.result = 'SUCCESS'
    } catch (exception) {
        currentBuild.result = 'FAILURE'
        throw exception
    } finally {
        // send message to Slack
        //    notifySlack(currentBuild.result)
    }
}

def updateBuildResources (cfg, ocpNamespace) {

    def openshiftNamespace = cfg.openshiftNamespaces[ocpNamespace]
    def buildParameters = [
        BUILD_NAME     : cfg.application.name,
        BUILD_LABEL    : cfg.application.label,
        DOCKER_REGISTRY: cfg.imageRepositoryUrl,
        IMAGE_NAME     : cfg.application.name,
        IMAGE_VERSION  : cfg.application.version
    ] << cfg.additionalBuildParams

    println "Creating build on $openshiftNamespace from config: $buildParameters"
    applyResourceFromTemplate(openshiftNamespace, 'configuration/templates/build-config.yaml', buildParameters)
}

def updateConfigMapResources (cfg, ocpNamespace) {

    def openshiftNamespace = cfg.openshiftNamespaces[ocpNamespace]

    def configMap = cfg.configMaps[ocpNamespace]
    def configMapParameters = [
        CONFIGMAP_NAME : configMap.name,
        CONFIGMAP_LABEL: configMap.label
    ] << configMap.params

    println "Creating config-map on $openshiftNamespace from config: $configMapParameters"
    applyResourceFromTemplate(openshiftNamespace, "configuration/templates/config-map.yaml", configMapParameters)
}

def importImage (cfg, ocpNamespace) {
    println "Importing image from: ${cfg.imageRepositoryUrl}/${cfg.application.name}:${cfg.application.version}"
    sh "oc import-image --from=${cfg.imageRepositoryUrl}/${cfg.application.name}:${cfg.application.version} --confirm ${cfg.application.name}:${cfg.application.version} --namespace ${cfg.openshiftNamespaces[ocpNamespace]}"
}

def updateDeploymentResources (cfg, ocpNamespace) {

    def openshiftNamespace = cfg.openshiftNamespaces[ocpNamespace]
    def imageStream = sh script: "oc get is ${cfg.application.name} --template {{.status.dockerImageRepository}} --namespace ${openshiftNamespace}", returnStdout: true

    def deploymentParameters = [
        DEPLOYMENT_NAME : cfg.application.name,
        DEPLOYMENT_LABEL: cfg.application.label,
        APPLICATION_NAME: cfg.application.name,
        IMAGE_STREAM    : imageStream,
        IMAGE_TAG       : cfg.application.version
    ] << cfg.additionalDeploymentParams

    println "Creating deployment on $openshiftNamespace from config: $deploymentParameters"
    applyResourceFromTemplate(openshiftNamespace, 'configuration/templates/deployment-config.yaml', deploymentParameters)
}

def updateServiceResources (cfg, ocpNamespace) {

    def openshiftNamespace = cfg.openshiftNamespaces[ocpNamespace]
    def serviceParameters = [
        SERVICE_NAME    : cfg.application.name,
        SERVICE_LABEL   : cfg.application.label,
        APPLICATION_NAME: cfg.application.name
    ] << cfg.additionalServiceParams

    println "Creating service on $openshiftNamespace from config: $serviceParameters"
    applyResourceFromTemplate(openshiftNamespace, 'configuration/templates/service-config.yaml', serviceParameters)
}

def updateRouteResources (cfg, ocpNamespace) {

    def openshiftNamespace = cfg.openshiftNamespaces[ocpNamespace]
    cfg.routes.each { route ->
        def routeParameters = [
            ROUTE_NAME  : route.name,
            ROUTE_LABEL : route.label,
            ROUTE_PATH  : route.path,
            SERVICE_NAME: route.service.name,
            SERVICE_PORT: route.service.port
        ]

        println "Creating route on $openshiftNamespace from config: $routeParameters"
        applyResourceFromTemplate(openshiftNamespace, 'configuration/templates/route-config.yaml', routeParameters)
    }
}

//def updateSecretResources(String openshiftNamespace) {
//
//    loginTo1Password()
//}
//
//
//def loginTo1Password() {
//
//    String url = getSecretValue(onePasswordSecretNamespace, onePasswordSecretName, 'url')
//    String email = getSecretValue(onePasswordSecretNamespace, onePasswordSecretName, 'email')
//    String secretKey = getSecretValue(onePasswordSecretNamespace, onePasswordSecretName, 'master_password') // Deliberate - stored incorrectly in OpenShift
//    String masterPassword = getSecretValue(onePasswordSecretNamespace, onePasswordSecretName, 'secret_key') // Deliberate - stored incorrectly in OpenShift
//
//    sh "echo ${masterPassword} | op signin ${url} ${email} ${secretKey} --output=raw"
//}
//
//
//def getSecretValue(String openshiftNamespace, String secretName, String keyName) {
//
//    String value = sh script: "oc get secret ${secretName} --output 'jsonpath={.data.${keyName}}' --namespace ${openshiftNamespace} | base64 --decode", returnStdout: true
//    return value
//}

@NonCPS
def applyResourceFromTemplate (openshiftNamespace, resourcePath, parameters) {

    def command = "oc process --filename ${resourcePath} --namespace ${openshiftNamespace}"
    parameters.each { key, value ->
        if (value != null) {
            command += " --param=${key}=${value}"
        }
    }
    command += " | oc apply --namespace ${openshiftNamespace} --filename -"
    sh "${command}"
}

def deployToOpenShift (cfg, ocpNamespace, replicas = 1) {

    def openshiftNamespace = cfg.openshiftNamespaces[ocpNamespace]

    sh "oc rollout latest dc/${cfg.application.name} --namespace ${openshiftNamespace}"
    sh "oc rollout status --watch dc/${cfg.application.name} --namespace ${openshiftNamespace}"

    if (replicas > 1) {
        sh "oc scale --replicas=${replicas} dc/${cfg.application.name} --namespace ${openshiftNamespace}"
    }

    sh "oc set triggers dc/${cfg.application.name} --from-config=true --namespace ${openshiftNamespace}"
}

def notifySlack (cfg, buildStatus = 'STARTED') {

    String colour
    switch (buildStatus) {
        case 'STARTED':
            colour = '#D4DADF'
            break
        case 'APPROVAL':
            colour = '#0072BC'
            break
        case 'SUCCESS':
            colour = '#BDFFC3'
            break
        case 'UNSTABLE':
            colour = '#FFFE89'
            break
        default:
            colour = '#FF9FA1'
            break
    }

    def resultBefore = currentBuild.result
    def lastBuildStatus = currentBuild.getPreviousBuild()?.getResult()

    // Only send if current build is not SUCCESS or current build is Back to Green following a failure
    boolean sendSlack = (buildStatus != 'SUCCESS' || (buildStatus == 'SUCCESS' && lastBuildStatus != 'SUCCESS'))
    if (sendSlack) {
        try {
            def message = "${buildStatus}: `${env.JOB_NAME}` #${env.BUILD_NUMBER}:\n<${env.JENKINS_URL}blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline|Click Here To View Pipeline>"
            if (buildStatus == 'SUCCESS') {
                message += "\nPrevious Build Status: ${lastBuildStatus}\nBuild is back to GREEN!"
            }
            slackSend(channel: cfg.slackChannel, color: colour, message: message)
        } catch (exception) {
            currentBuild.result = resultBefore
        }
    }
}

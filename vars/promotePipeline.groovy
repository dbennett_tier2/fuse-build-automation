#!/usr/bin/env groovy

def defaultConfig(name, label) {
    return [
        application: [
            name: 'find-my-nearest',
            label: 'web-service'
        ],
        routes: [
            [
                name: name,
                label: label,
                path: '/',
                service: [
                    name: name,
                    port: '10000'
                ]
            ]
        ],
        source: [
            repositoryPath: "bitbucket.org/motabilityoperations/fuse-${name}.git",
            repositoryBranch: "refs/tags/${env.IMAGE_VERSION}",
            repositoryCredentials: 'bitbucket-ro',
            imageRepositoryUrl: "nexus-${env.SOURCE_ENVIRONMENT}-devops.services.theosmo.com/mo",
            imageRepositoryCredentials: 'docker-ro'
        ],
        target: [
            imageRepositoryUrl: "nexus-${env.TARGET_ENVIRONMENT}-devops.services.theosmo.com/mo",
            imageRepositoryCredentials: 'docker-rw'
        ],
        openshiftNamespace: "fuse-${env.TARGET_ENVIRONMENT}",
        slackChannel: '#fuse-builds-status'
    ]
}

def promote(cfg, currentBuild) {
    try {
        node('mo-aio') {
            // get latest version of source code from source control for template files
            stage('Checkout Source') {
                checkout scm: [
                    $class: 'GitSCM',
                    userRemoteConfigs: [[
                        url: "https://${cfg.source.repositoryPath}",
                        credentialsId: cfg.source.repositoryCredentials
                    ]],
                    branches: [[
                        name: "${cfg.source.repositoryBranch}"
                    ]]
                ], poll: false
            }
            // copy image from source image repository to target image repository
            stage('Copy Image') {
                withCredentials([usernamePassword(credentialsId: cfg.source.imageRepositoryCredentials,
                    usernameVariable: 'sourceImageRepositoryUsername',
                    passwordVariable: 'sourceImageRepositoryPassword'),
                         usernamePassword(credentialsId: cfg.target.imageRepositoryCredentials,
                             usernameVariable: 'targetImageRepositoryUsername',
                             passwordVariable: 'targetImageRepositoryPassword')
                ]) {
                    Map<String, Object> jobParameters = [
                        RESOURCE_NAME         : "${cfg.application.name}-${env.IMAGE_VERSION}",
                        IMAGE                 : cfg.application.name,
                        IMAGE_VERSION         : env.IMAGE_VERSION,
                        SOURCE_DOCKER_REGISTRY: cfg.source.imageRepositoryUrl,
                        SOURCE_CREDENTIALS    : "${sourceImageRepositoryUsername}:${sourceImageRepositoryPassword}",
                        TARGET_DOCKER_REGISTRY: cfg.target.imageRepositoryUrl,
                        TARGET_CREDENTIALS    : "${targetImageRepositoryUsername}:${targetImageRepositoryPassword}"
                    ]
                    applyResourceFromTemplate(cfg.openshiftNamespace, 'configuration/templates/skopeo-job.yaml', jobParameters)
                }
            }
        }
        currentBuild.result = 'SUCCESS'
    } catch (exception) {
        currentBuild.result = 'FAILURE'
        throw exception
    } finally {
        // send message to Slack
//    notifySlack(cfg, currentBuild, currentBuild.result)
    }
}

@NonCPS
def applyResourceFromTemplate(String openshiftNamespace, String resourcePath, Map< String, Object> parameters) {

    String command = "oc process --filename ${resourcePath} --namespace ${openshiftNamespace}"
    parameters.each { String key, Object value ->
        if (value != null) {
            command += " --param=${key}=${value}"
        }
    }
    command += " | oc apply --namespace ${openshiftNamespace} --filename -"
    sh "${command}"
}

def notifySlack(cfg, currentBuild, buildStatus = 'STARTED') {

    String colour
    switch (buildStatus) {
        case 'STARTED':
            colour = '#D4DADF'
            break
        case 'APPROVAL':
            colour = '#0072BC'
            break
        case 'SUCCESS':
            colour = '#BDFFC3'
            break
        case 'UNSTABLE':
            colour = '#FFFE89'
            break
        default:
            colour = '#FF9FA1'
            break
    }

    def resultBefore = currentBuild.result
    def lastBuildStatus = currentBuild.getPreviousBuild()?.getResult()

    // Only send if current build is not SUCCESS or current build is Back to Green following a failure
    boolean sendSlack = (buildStatus != 'SUCCESS' || (buildStatus == 'SUCCESS' && lastBuildStatus != 'SUCCESS'))
    if (sendSlack) {
        try {
            String message = "${buildStatus}: `${env.JOB_NAME}` #${env.BUILD_NUMBER}:\n<${env.JENKINS_URL}blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline|Click Here To View Pipeline>"
            if (buildStatus == 'SUCCESS') {
                message += "\nPrevious Build Status: ${lastBuildStatus}\nBuild is back to GREEN!"
            }
            slackSend(channel: cfg.slackChannel, color: colour, message: message)
        } catch (exception) {
            currentBuild.result = resultBefore
        }
    }
}
